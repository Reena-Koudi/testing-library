import { Component } from '@angular/core';

@Component({
  selector: 'testing-library-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular-unit-testing';
}