import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'testing-library-take-a-pic',
  templateUrl: './take-a-pic.component.html',
  styleUrls: ['./take-a-pic.component.css']
})
export class TakeAPicComponent implements OnInit {
  show = false;
  constructor() { }

  ngOnInit() {
  }
  onShow() {
    this.show = !this.show;
  }

}
