import { FilmsService } from './films.service';
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FILMS } from '../../models/mock-data';



describe('Films Service', () => {
    let testBed: TestBed;
    let service: FilmsService;
    let httpMock: HttpTestingController;
    const films = FILMS;

    beforeEach(() => {

        TestBed.configureTestingModule({

            imports: [HttpClientTestingModule],

            providers: [FilmsService]

        });

        testBed = getTestBed();

        service = testBed.get(FilmsService);

        httpMock = testBed.get(HttpTestingController);

    });

    it('#getAllFilms should return list of films', () => {

        service.getAllFilms().subscribe(data => {
        expect(data.length).toBe(3);
        expect(data[1].name).toBe('Tron: Legacy');
        });

        const req = httpMock.expectOne(`${service.filmUrl}`);

        expect(req.request.method).toBe('GET');

        req.flush(films);

        httpMock.verify();
    });

    it('#getFilmById should get correct data for specified id', () => {
        service.getFilmById(9).subscribe((data: any) => {
        expect(data.name).toBe('Crazy Rich Asians');
        });

        const req = httpMock.expectOne(`${service.filmUrl}/9`);

        expect(req.request.method).toBe('GET');

        req.flush(films[2]);

        httpMock.verify();
    });

    it('#addFilm() should post the correct data', () => {
        const film: any = {
                    'name': 'Trolls',
                    'id': 34,
                    'year': '2016',
                    'image': 'https://images-na.ssl-images-amazon.com/images/I/A1AHFqHWgmL._RI_SX300_.jpg'
                  };
      service.addFilm(film).subscribe(data => {
        expect(data).toBe(film);
        });

        const req = httpMock.expectOne(`${service.filmUrl}`);
        expect(req.request.method).toBe('POST');

        req.flush(film);

        httpMock.verify();
      });

      it('#editFilm should put the correct data', () => {
        const film: any = {
            'name': 'Moana',
            'id': 10,
            'year': '2016',
            'image': 'https://images-na.ssl-images-amazon.com/images/I/91kkX9p5H0L._SY679_.jpg'
    };
        service.editFilm(film).subscribe(data => {
            expect(data).toBe(film);
        });
        const req = httpMock.expectOne(`${service.filmUrl}/10`);
        req.flush(film);

        httpMock.verify();
      });

      it('#deleteFilm should delete the film from the list', () => {
        service.deleteFilm(5).subscribe(filmData => {
        expect(filmData).toBe(5);
        });
        const req = httpMock.expectOne(`${service.filmUrl}/5`);

        expect(req.request.method).toBe('DELETE');

        req.flush(5);

        httpMock.verify();
    });
    });



