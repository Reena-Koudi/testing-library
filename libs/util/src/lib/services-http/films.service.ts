import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Film } from '../../models/film';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FilmsService {

  filmUrl = 'http://localhost:3000/films';

  constructor(private http: HttpClient) { }

  getAllFilms(): Observable<Film[]> {
    return this.http.get<Film[]>(this.filmUrl);
  }

  getFilmById(id: number): Observable<Film> {
    const url = `${this.filmUrl}/${id}`;
    return this.http.get<Film>(url);
  }

  addFilm (film: Film) {
    return this.http.post<Film>(this.filmUrl, film);
  }

  editFilm (film: Film) {
    return this.http.put(this.filmUrl + '/' + film.id, film);
  }

  deleteFilm(id: number) {
    return this.http.delete(this.filmUrl + '/' + id);
  }

}
