import { Output, Component, EventEmitter } from '@angular/core';

@Component({
  selector: 'testing-library-output',
  templateUrl: './output.component.html'
})
export class OutputComponent {
  @Output() greet: EventEmitter<string> = new EventEmitter<string>();

  doGreet() {
    this.greet.emit('Hi');
  }
}