import { Component, Input } from "@angular/core";

@Component({
selector: 'testing-library-input',
templateUrl: './input.component.html'
})

export class InputComponent {
    @Input() message: string;
}