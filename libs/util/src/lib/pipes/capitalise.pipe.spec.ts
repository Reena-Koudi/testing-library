import { TestBed, inject } from "@angular/core/testing";
import { CapitalisePipe } from './capitalise.pipe';

describe('Pipe: CapitalisePipe', () => {
    let pipe;
    
    //setup
    beforeEach(() => TestBed.configureTestingModule({
      providers: [ CapitalisePipe ]
    }));
    
    beforeEach(inject([CapitalisePipe], p => {
      pipe = p;
    }));
    
    //specs
    it('should work with empty string', () => {
      expect(pipe.transform('')).toEqual('');
    });
    
    it('should capitalise', () => {
      expect(pipe.transform('wow')).toEqual('WOW');
    });
    
    it('should throw with invalid values', () => {
      //must use arrow function for expect to capture exception
      expect(()=>pipe.transform(undefined)).toThrow();
      expect(()=>pipe.transform()).toThrow();
      expect(()=>pipe.transform()).toThrowError('Requires a String as input');
    });
  }) 