import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(list: any[], searchText: string): any[] {
    if (!list) { return []; }
    if (!searchText) { return list; }
    searchText = searchText.toLowerCase();
    // console.log('filter list', list);
    return list.filter(item => {
      // return item.name.toLowerCase().includes(searchText);
      return  Object.keys(item).some(key => item[key] != null && item[key].toString().toLowerCase().includes(searchText));
    });
  }
}


