import { TestBed, inject } from '@angular/core/testing';
import { FilterPipe } from './filter.pipe';
import { FILMS } from '../../models/mock-data';


describe('Pipe: FilterPipe', () => {
let pipe;
const films = FILMS;

beforeEach(() => TestBed.configureTestingModule({
  providers: [ FilterPipe ]
}));

beforeEach(inject([FilterPipe], p => {
pipe = p;
}));


it('should return empty array if no list', () => {
    expect(pipe.transform([], 'Mo')).toEqual([]);
});

it('should return list if no searchtext', () => {
  expect(pipe.transform(FILMS)).toEqual(FILMS);
});

it('should return the filtered list', () => {
   expect(pipe.transform(FILMS, 'mo')).toEqual([FILMS[0]]);
});

it('should return empty array if searchtext not found', () => {
   expect(pipe.transform(FILMS, 'xyz')).toEqual([]);
});
});
