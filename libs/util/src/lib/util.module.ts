import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputComponent } from './components/input/input.component';
import { OutputComponent } from './components/output/output.component';
import { DomtestingComponent } from './components/domtesting/domtesting.component';
import { HoverFocusDirective } from './directives/hoverfocus.directive';
import { LogClickDirective } from './directives/logclick.directive';
import { Container } from './directives/logclick.directive.spec';

@NgModule({
  imports: [CommonModule],
  declarations: [InputComponent,
                OutputComponent,
                DomtestingComponent,
                HoverFocusDirective,
                LogClickDirective,
                Container]
})
export class UtilModule {}
