import { Component, Output, EventEmitter } from '@angular/core';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { LogClickDirective } from './logclick.directive';

@Component({ 
    selector: 'testing-library-container',
    template: `<div log-clicks (changes)="changed($event)"></div>`
  })
  export class Container {  
    @Output() changes = new EventEmitter();
    
    changed(value){
      this.changes.emit(value);
    }
  }
  
  describe('Directive: logClicks', () => {
    let fixture;
    let container;
    let element;  
  
    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ Container, LogClickDirective ]
      });
  
      fixture = TestBed.createComponent(Container);
      container = fixture.componentInstance; // to access properties and methods
      element = fixture.nativeElement;       // to access DOM element
    });
    
    it('should increment counter', fakeAsync(() => {
      const div = element.querySelector('div');
      //set up subscriber
      container.changes.subscribe(x => { 
        expect(x).toBe(1);
      });
      //trigger click on container
      div.click();
      //execute all pending asynchronous calls
      tick();
    }));
  }) 