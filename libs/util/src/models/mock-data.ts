import { Film } from '../models/film';
export const FILMS: Film[] = [
    {
        'name': 'Moana',
        'id': 5,
        'year': '2016',
        'image': 'https://images-na.ssl-images-amazon.com/images/I/91kkX9p5H0L._SY679_.jpg'
    },
    {
        'name': 'Tron: Legacy',
        'id': 3,
        'year': 2010,
        'image': 'http://t1.gstatic.com/images?q=tbn:ANd9GcSchLICLsn0n_GlVBYmaIZjwcYDuqvb1fg4nVvr8WAh3FN8EqfY'
    },
    {
        'name': 'Crazy Rich Asians',
        'id': 9,
        'year': '2018',
        'image': 'https://assets.nflxext.com/us/boxshots/hd1080/80239019.jpg'
    },
];
