export interface Film {
    id: number;
    name: string;
    year: number | string;
    image: string;
}
